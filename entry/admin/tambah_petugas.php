
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Suki|| Sistem Untuk Keperluan Inventaris</title>

  <!-- ========== Css Files ========== -->
  <link href="css/root.css" rel="stylesheet">

  </head>
  <body>
  <!-- Start Page Loading -->
 <div class="loading"><img src="img/loading.gif" alt="loading-img"></div>
 
 
  <!-- End Page Loading -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 

  </div>
  <!-- END TOP -->
   <?php 
  
session_start();


 
  // cek apakah yang mengakses halaman ini sudah login
  if($_SESSION['id_level']==""){
    header("location:../../index.php?pesan=gagal");
  }
 
  ?>
  
<!-- START SIDEBAR -->


  <?php include "page/header.php"; ?>
      <!-- START SIDEBAR -->
  <?php include "page/sidebar.php"; ?>


<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Tambah Petugas</h1>
      <ol class="breadcrumb">
        <li><a href="index.html">Dashboard</a></li>
        <li><a href="#">Data petugas</a></li>
        <li><a href="#">Tambah petugas</a></li>
        
      </ol>


  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">

    <!-- Start Panel -->
    <div class="col-md-12">
      <div class="panel panel-default"> 
<div class="row">
   <div class="col-md-15">
      <div class="panel panel-widget">
<form method="post" action="proses/proses_tambah_petugas.php">
  <div class="form-group">
    <label >Nama</label>
    <input type="text" name="nama_petugas" class="form-control" >
  </div>
  <div class="form-group">
    <label >Username</label>
    <input type="text" name="username" class="form-control"  >
  </div> 
  <div class="form-group">
    <label>Password</label>
    <input type="password" name="password" class="form-control">
  </div>
    <?php
        include "koneksi.php";
        $result = mysqli_query($conn,"select * from level order by id_level asc ");
        $jsArray = "var id_jenis = new Array();\n";
        ?>  
        <label>Level</label>
        <select class="form-control" name="id_level" onchange="changeValue(this.value)">
        <option selected="selected">
        <?php 
        while($row = mysqli_fetch_array($result)){
                    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
          $jsArray .= "id_level['". $row['id_level']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
        }
                  ?>
        </option>
        </select> 
  
    
  <button type="submit" class="btn btn-primary" value="simpan">Save changes</button>
</form>
                      </div>
                    </div>
</div>

      </div>
    </div>
    <!-- End Panel -->

  </div>
  <!-- End Row -->
</div>

  <?php include "page/footer.php"; ?>



</div>
<!-- End Content -->
 
<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>
<script type="text/javascript"> 
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
</script>


<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>


</body>

</html>