
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Beranda || Sistem Untuk Keperluan Inventaris</title>

  <!-- ========== Css Files ========== -->
  <link href="../css/root.css" rel="stylesheet">

  </head>
  <body>
  <!-- Start Page Loading -->
 
  <!-- End Page Loading -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 

  </div>
  <!-- END TOP -->

<!-- START SIDEBAR -->
   <?php 
  
session_start();


 
  // cek apakah yang mengakses halaman ini sudah login
  if($_SESSION['id_level']==""){
    header("location:../../index.php?pesan=gagal");
  }
 
  ?>
  

  <?php include "../page/header.php"; ?>
      <!-- START SIDEBAR -->
  <?php include "../page/sidebar.php"; ?>


<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Data Inventaris</h1>
      <ol class="breadcrumb">
        <li><a href="index.html">Dashboard</a></li>
        <li><a href="#">Data inventaris</a></li>
        
      </ol>


  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">

    <!-- Start Panel -->
    <div class="col-md-12">
      <div class="panel panel-default">
<?php
include "koneksi.php";

$id_pegawai=$_GET['id_pegawai'];
$select=mysqli_query($conn,"select * from pegawai where id_pegawai='$id_pegawai'");
$tampil=mysqli_fetch_array($select);
?>  

<div class="row">
   <div class="col-md-15">
      <div class="panel panel-widget">
            <form method="post" action="update_pegawai.php?id_pegawai=<?php echo $id_pegawai;?>">
  <div class="form-group">
    <label >Nama</label>
    <input type="text" name="nama_pegawai" value="<?php echo $tampil['nama_pegawai']; ?>" class="form-control" >
  </div>
  <div class="form-group">
    <label >NIP</label>
    <input type="text" name="nip" value="<?php echo $tampil['nip']; ?>" class="form-control"  >
  </div> 
  <div class="form-group">
    <label>Alamat</label>
    <input type="text" name="alamat" value="<?php echo $tampil['alamat']; ?>" class="form-control">
  </div>
  
    
  <button type="submit" class="btn btn-primary" name="simpan" value="simpan">Save changes</button>
</form>
                      </div>
                    </div>
</div>

      </div>
    </div>
    <!-- End Panel -->

  </div>
  <!-- End Row -->
</div>

  <?php include "../page/footer.php"; ?>



</div>
<!-- End Content -->
 
<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>
<script type="text/javascript"> 
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
</script>


<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>


</body>

</html>