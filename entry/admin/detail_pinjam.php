
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Suki || Sistem Untuk Keperluan Inventaris</title>

  <!-- ========== Css Files ========== -->
  <link href="css/root.css" rel="stylesheet">
  <link href="css/sweetalert.css" rel="stylesheet">
  </head>
  <body>
  <!-- Start Page Loading -->
 
  <!-- START SIDEBAR -->
   <?php 
  
session_start();


 
  // cek apakah yang mengakses halaman ini sudah login
  if($_SESSION['id_level']==""){
    header("location:../../index.php?pesan=gagal");
  }
 
  ?>
  

  <?php include "page/header.php"; ?>
      <!-- START SIDEBAR -->
  <?php include "page/sidebar.php"; ?>


<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Tambah Inventaris</h1>
      <ol class="breadcrumb">
        <li><a href="home">Dashboard</a></li>
        <li><a href="#">Data inventaris</a></li>
        <li><a href="#">Tambah inventaris</a></li>
        
      </ol>


  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
      
<div class="row">
   <div class="col-md-6">
      <div class="panel panel-widget">
<form method="post" action="proses/proses_detail_pinjam.php">
 <?php
  include "koneksi.php";
  $result = mysqli_query($conn,"select * from inventaris order by id_inventaris asc ");
  $jsArray = "var id_inventaris = new Array();\n";
  ?>  
 <label>Nama inventaris</label>
  <select class="form-control" name="id_inventaris" onchange="changeValue(this.value)" required="">
  <option selected="selected">
   <?php 
   while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[2]</option>";
    $jsArray .= "id_inventaris['". $row['id_inventaris']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
        }
                  ?>
        </option>
        </select>
 

   <div class="form-group">
    <label >Jumlah</label>
    <input type="text" id="t1" name="jumlahp" class="form-control" 
     onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
     onkeyup   ="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required="">
     
  </div>
  <div class="form-group">
    <label >ID Peminjaman </label>
   <?php 
        $id_peminjaman = $_GET['id_peminjaman'];
        include"koneksi.php";
        $select=mysqli_query($conn, "SELECT * FROM peminjaman where id_peminjaman='$id_peminjaman'");
        while ($show=mysqli_fetch_array($select)) {
        
    ?>
    <input type="text" name="id_peminjaman" value="<?php echo $show['id_peminjaman']?>"" class="form-control" required="" readonly>
  </div><?php } ?>
 
    <br>
  <button type="submit" class="btn btn-primary"  value="simpan">Save changes</button>
</form>
                      </div>
                    </div>

 <div class="col-md-1 col-lg-6">
      <div class="panel panel-widget" style="height:450px;">
        <div class="panel-title">
          Data Barang  <span class="label label-danger"></span>
          <ul class="panel-tools">
            <li><a class="icon"><i class="fa fa-refresh"></i></a></li>
            <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
          </ul>
        </div>
        <div class="panel-body table-responsive">

          <table class="table table-dic table-hover ">
            <tbody>
                 <table id="example0" class="table display">
                <thead>
                  <tr>
                  
                    <td align="center">Kode Inventaris</td>
                    <td align="center">Nama Barang</td>
                    <td align="center">Jumlah</td>
                    
                    <td align="center">Opsi</td>
                  </tr>

<?php
              $no= 1;
              $ambil_data="SELECT * from inventaris order by id_inventaris desc";
              $sql = mysqli_query($conn, $ambil_data);
              while ($hasil = mysqli_fetch_array($sql)) 
              {
?>
             </thead>
                
                    <tr>
                      
                      <td align="center"><?php echo $hasil['kode_inventaris'] ?></td>
                      <td align="center"><?php echo $hasil['nama'] ?></td>
                      <td align="center"><?php echo $hasil['jumlah'] ?></td>
                    
                      <td align="center">
 <a href="edit_inventaris?id_inventaris=<?php echo $hasil['id_inventaris']; ?>" class="btn btn-rounded btn-danger btn-icon"><i class="fa fa-edit"></i></a>
                      </td>
                    </tr>
 
                 <?php
  }
  ?>

            </table>
           
            </tbody>
          </table>          

        </div>
      </div>
    </div>

</div>

      
    
   
  <!-- End Row -->
</div>

  <?php include "page/footer.php"; ?>



</div>
<!-- End Content -->
 
 <script type="text/javascript" src="../js/sweetalert.js"></script>
<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>
<script type="text/javascript" src="js/sweetalert.js"></script>


<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>
<script type="text/javascript"> 
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
</script>


<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>


      
<script type="text/javascript" language=JavaScript>
function inputDigitsOnly(e) {
 var chrTyped, chrCode=0, evt=e?e:event;
 if (evt.charCode!=null)     chrCode = evt.charCode;
 else if (evt.which!=null)   chrCode = evt.which;
 else if (evt.keyCode!=null) chrCode = evt.keyCode;

 if (chrCode==0) chrTyped = 'SPECIAL KEY';
 else chrTyped = String.fromCharCode(chrCode);

 //[test only:] display chrTyped on the status bar 
 self.status='inputDigitsOnly: chrTyped = '+chrTyped;

 //Digits, special keys & backspace [\b] work as usual:
 if (chrTyped.match(/\d|[\b]|SPECIAL/)) return true;
 if (evt.altKey || evt.ctrlKey || chrCode<28) return true;

 //Any other input? Prevent the default response:
 if (evt.preventDefault) evt.preventDefault();
 evt.returnValue=false;
 return false;
}

function addEventHandler(elem,eventType,handler) {
 if (elem.addEventListener) elem.addEventListener (eventType,handler,false);
 else if (elem.attachEvent) elem.attachEvent ('on'+eventType,handler); 
 else return 0;
 return 1;
}

// onload: Call the init() function to add event handlers!
function init() {
 addEventHandler(self.document.f2.elements[0],'keypress',inputDigitsOnly);
 addEventHandler(self.document.f2.elements[1],'keypress',inputDigitsOnly);
}

</script>

</body>

</html>