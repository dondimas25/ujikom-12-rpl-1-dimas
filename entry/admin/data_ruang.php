<?php
include "koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from egemem.com/theme/kode/v1.1/data-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Jan 2019 01:47:17 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Suki.com - Sistem Inventaris </title>

  <!-- ========== Css Files ========== -->
  <link href="css/root.css" rel="stylesheet">

  </head>
  <body>
  <!-- Start Page Loading -->
  <div class="loading"><img src="img/load.gif" alt="loading-img"></div>
  <!-- End Page Loading -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 

  </div>
        <?php 
  
session_start();


 
  // cek apakah yang mengakses halaman ini sudah login
  if($_SESSION['id_level']==""){
    header("location:../../index.php?pesan=gagal");
  }
 
  ?>
  
  
  <?php include "page/header.php"; ?>
      <!-- START SIDEBAR -->
    <?php include "page/sidebar.php"; ?>


<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Data Ruang</h1>
      <ol class="breadcrumb">
        <li><a href="index.html">Dashboard</a></li>
        <li><a href="#">Data Ruang</a></li>
        
      </ol>

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">

    <!-- Start Panel -->
    <div class="col-md-12">
      
      <div class="panel panel-default">
     
                      <div class="panel-body table-responsive">
                           <a href="tambah_ruang.php" class="btn btn-danger"> Tambah</a><br><br>
                          <table id="example0" class="table display">
                              <thead>
                                   <tr>
                    <td align="center">No</td>
                    <td align="center">Nama Ruang</td>
                    <td align="center">Kode Ruang</td>
                    <td align="center">Keterangan</td>
                    <td align="center">Opsi</td>
                  </tr>

<?php
              $no= 1;
              $ambil_data="SELECT * from ruang order by id_ruang desc";
              $sql = mysqli_query($conn, $ambil_data);
              while ($hasil = mysqli_fetch_array($sql)) 
              {
?>
             </thead>
            
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td align="center"><?php echo $hasil['nama_ruang'] ?></td>
                      <td align="center"><?php echo $hasil['kode_ruang'] ?></td>
                      <td align="center"><?php echo $hasil['keterangan'] ?></td>
                      <td align="center">
                       <a href="edit_ruang?id_ruang=<?php echo $hasil['id_ruang']; ?>" class="btn btn-rounded btn-danger btn-icon"><i class="fa fa-edit"></i></a>
                      </td>
                    </tr>
  <?php
  }
  ?>
                              
                          </table>


                      </div>

      </div>
    </div>
    <!-- End Panel -->

  </div>
  <!-- End Row -->
</div>

  <?php include "page/footer.php"; ?>



</div>
<!-- End Content -->
 
<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>



<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="9">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
} );
</script>

</body>

</html>