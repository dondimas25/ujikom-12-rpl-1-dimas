<div class="sidebar clearfix">

<ul class="sidebar-panel nav">
  <li class="sidetitle">MAIN</li>
  <li><a href="home.php"><span class="icon color5"><i class="fa fa-home"></i></span>Dashboard</a></li>
</ul>

<ul class="sidebar-panel nav">
  <li class="sidetitle" >MASTER DATA</li>
 </li>
<li><a href="data_inventaris.php"><span class="icon color5"><i class="fa fa-cubes"></i></span>Data Inventaris</a></li> 
<li><a href="data_ivn_ruangan.php"><span class="icon color5"><i class="fa fa-briefcase"></i></span>Data Inventaris Ruangan</a></li> 
<li><a href="data_jenis"><span class="icon color8"><i class="fa  fa-folder-o"></i></span>Data Jenis</a></li>
  <li><a href="data_ruang.php"><span class="icon color6"><i class="fa fa-institution"></i></span>Data Tempat/Ruang</a></li>
  <li><a href="data_petugas.php"><span class="icon color6"><i class="fa fa-user"></i></span>Data Petugas</a></li>
  <li><a href="data_peminjam.php"><span class="icon color6"><i class="fa fa-folder"></i></span>Data Pinjam</a></li>
  <li><a href="data_pegawai.php"><span class="icon color6"><i class="fa fa-user"></i></span>Data Pegawai</a></li>
  </ul>
  <ul class="sidebar-panel nav">
  <li><a href="peminjaman.php"><span class="icon color6"><i class="fa fa-desktop"></i></span> Peminjaman</a></li>
   <li><a href="#"><span class="icon color14"><i class="fa fa-print"></i></span>Generate Laporan<span class="caret"></span></a>
    <ul>
      <li><a href="social-profile.html">Social Profile</a></li>
      <li><a href="invoice.html">Invoice<span class="label label-danger">NEW</span></a></li>
      <li><a href="login.html">Login Page</a></li>
      <li><a href="register.html">Register</a></li>
      <li><a href="forgot-password.html">Forgot Password</a></li>
      <li><a href="lockscreen.html">Lockscreen</a></li>
      <li><a href="blank.html">Blank Page</a></li>
      <li><a href="contact.html">Contact</a></li>
      <li><a target="_blank" href="Landing/index.html">Front-end Template</a></li>
      <li><a href="404.html">404 Page</a></li>
      <li><a href="500.html">500 Page</a></li>
    </ul>
  </li>
  <li><a href="backup.php"><span class="icon color6"><i class="fa fa-desktop"></i></span> Backup Database</a></li>

  </ul>

<ul class="sidebar-panel nav">
   <li><a href="../../"><span class="icon color6"><i class="fa fa-sign-out-alt"></i></span>Logout </a></li>
</ul>
</div>