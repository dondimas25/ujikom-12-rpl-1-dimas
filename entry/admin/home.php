<?php include 'koneksi.php' ?>

<!DOCTYPE html>
<html lang="en">
  
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Beranda || Administrator Sistem Untuk Keperluan  Inventaris </title>
  <link href="css/root.css" rel="stylesheet">
  <link href="css/sweetalert.css" rel="stylesheet">


  </head>
  <body>
  <div class="loading"><img src="img/load.gif" alt="loading-img"></div>

      <?php 
  
session_start();


 
  // cek apakah yang mengakses halaman ini sudah login
  if($_SESSION['id_level']==""){
    header("location:../../index.php?pesan=gagal");
  }
 
  ?>
  

  <?php include "page/header.php"; ?>
      <!-- START SIDEBAR -->
  <?php include "page/sidebar.php"; ?>
<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">

    <h1 class="title">Dashboard</h1>
      <ol class="breadcrumb">
       
    </ol>

  

  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-widget">


 <div class="col-md-12">
  <ul class="topstats clearfix">
    <li class="arrow"></li>
    <li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-dot-circle-o"></i> Data Inventaris</span>
      <h3><?php
                    $ambil_data="SELECT * from inventaris";
                    $sql = mysqli_query($conn, $ambil_data);
                    $hasil = mysqli_num_rows($sql);
                    echo $hasil;
                  ?></h3>
    
    </li>
    <li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-calendar-o"></i> Data Ruang</span>
      <h3><?php
                    $ambil_data="SELECT * from ruang";
                    $sql = mysqli_query($conn, $ambil_data);
                    $hasil = mysqli_num_rows($sql);
                    echo $hasil;
                  ?></h3></h3>
          </li>
    <li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-shopping-cart"></i> Data Jenis</span>
      <h3 class="color-up"><?php
                    $ambil_data="SELECT * from jenis";
                    $sql = mysqli_query($conn, $ambil_data);
                    $hasil = mysqli_num_rows($sql);
                    echo $hasil;
                  ?></h3></h3>
      
          </li>
    <li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-users"></i> Data Pegawai</span>
      <h3><?php
                    $ambil_data="SELECT * from pegawai";
                    $sql = mysqli_query($conn, $ambil_data);
                    $hasil = mysqli_num_rows($sql);
                    echo $hasil;
                  ?></h3></h3>
      
    </li>
    <li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-eye"></i> Data petugas</span>
      <h3 class="color-up"><?php
                    $ambil_data="SELECT * from petugas";
                    $sql = mysqli_query($conn, $ambil_data);
                    $hasil = mysqli_num_rows($sql);
                    echo $hasil;
                  ?></h3></h3>
      
    </li>
    <li class="col-xs-6 col-lg-2">
      <span class="title"><i class="fa fa-clock-o"></i> Data Peminjam</span>
      <h3 class="color-up"><?php
                    $ambil_data="SELECT * from detail_pinjam";
                    $sql = mysqli_query($conn, $ambil_data);
                    $hasil = mysqli_num_rows($sql);
                    echo $hasil;
                  ?></h3></h3>
    </li>
  </ul>
  </div>
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">
   <div class="col-md-12 ">
      <div class="panel panel-widget">
        <div class="panel-title">
          Data Inventaris <span class="label label-danger">9</span>
          <ul class="panel-tools">
            <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
            <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
            <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
          </ul>
        </div>
        <div class="panel-body table-responsive">

            <table id="example" class="table display">
                <thead>
                  <tr>
                    <td align="center">Kode Inventaris</td>
                    <td align="center">Nama Barang</td>
                    <td align="center">Jenis</td>
                    <td align="center">Kondisi</td>
                    <td align="center">Ruang</td>
                    <td align="center">Tanggal Register</td>
                    <td align="center">Petugas</td>
                    <td align="center">Sumber</td>
                     <td align="center">Keterangan</td>
                        
                  </tr>

<?php
              $no= 1;
              $ambil_data="SELECT * from inventaris  order by id_inventaris desc";
              $sql = mysqli_query($conn, $ambil_data);
              while ($hasil = mysqli_fetch_array($sql)) 
              {
?>
             </thead>
                
                    <tr>
                      <td align="center"><?php echo $hasil['kode_inventaris'] ?></td>
                      <td align="center"><?php echo $hasil['nama'] ?></td>
                      <td align="center"><?php echo $hasil['id_jenis'] ?></td>
                      <td align="center"><?php echo $hasil['kondisi'] ?></td>
                      <td align="center"><?php echo $hasil['id_ruang'] ?></td>
                       <td align="center"><?php echo $hasil['tanggal_register'] ?></td>
                      <td align="center"><?php echo $hasil['id_petugas'] ?></td>
                       <td align="center"><?php echo $hasil['sumber'] ?></td>
                       <td align="center"><?php echo $hasil['keterangan'] ?></td>
                    
                    </tr>
 
                 <?php
  }
  ?>

            </table>


        </div>
      </div>
    </div>
    <!-- End Panel -->

  </div>
  <!-- End Row -->
</div>
<!-- Start Footer -->
<?php include "page/footer.php"; ?>
<!-- End Footer -->


</div>


<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>



<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="9">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
} );
</script>


</body>

</html>
