
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Beranda || Sistem Untuk Keperluan Inventaris</title>

  <!-- ========== Css Files ========== -->
  <link href="../css/root.css" rel="stylesheet">
  <link href="../css/sweetalert.css" rel="stylesheet">
  </head>
  <body>
  <!-- Start Page Loading -->
 
  <!-- End Page Loading -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 

  </div>
  <!-- END TOP -->

<!-- START SIDEBAR -->


  <?php include "../page/header.php"; ?>
      <!-- START SIDEBAR -->
  <?php include "../page/sidebar.php"; ?>


<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Data Inventaris</h1>
      <ol class="breadcrumb">
        <li><a href="index.html">Dashboard</a></li>
        <li><a href="#">Data inventaris</a></li>
        
      </ol>


  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">

    <!-- Start Panel -->
    <div class="col-md-12">
      <div class="panel panel-default"> 
<div class="row">
   <div class="col-md-15">
      <div class="panel panel-widget">
<form method="post" action="proses_tambah_inventaris.php">
  <div class="form-group">
    <label >Nama</label>
    <input type="text" name="nama" class="form-control" required="">
  </div>

  <div class="form-group">
    <label >Tanggal pinjam</label>
    <input type="text" name="tanggal_pinjam" value="<?php echo date('d-m-y ')?>" class="form-control" required="">
  </div>
  <div class="form-group">
    <label >Tanggal balik</label>
    <input type="date" name="tanggal_balik" class="form-control" required="">
  </div>
   <div class="form-group">
    <label>status peminjaman</label>
    <input type="text" name="stpinjam" class="form-control" required="">
  </div>
    <?php
  include "koneksi.php";
  $result = mysqli_query($conn,"select * from pegawai order by id_pegawai asc ");
  $jsArray = "var id_pegawai = new Array();\n";
  ?>  
  <label>Petugas</label>
  <select class="form-control" name="id_pegawai" onchange="changeValue(this.value)" required="">
  <option selected="selected">
   <?php 
   while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "id_pegawai['". $row['id_pegawai']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
        }
                  ?>
        </option>
        </select> 
  
    
  <button type="submit" class="btn btn-primary" onclick="validation()" value="simpan">Save changes</button>
</form>
                      </div>
                    </div>
</div>

      </div>
    </div>
    <!-- End Panel -->

  </div>
  <!-- End Row -->
</div>

  <?php include "../page/footer.php"; ?>



</div>
<!-- End Content -->
 
 <script type="text/javascript" src="../js/sweetalert.js"></script>
<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>
<script type="text/javascript" src="js/sweetalert.js"></script>


<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>
<script type="text/javascript"> 
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
</script>


<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>


</body>

</html>