
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Beranda || Sistem Untuk Keperluan Inventaris</title>

  <!-- ========== Css Files ========== -->
  <link href="css/root.css" rel="stylesheet">

  </head>
  <body>
  <!-- Start Page Loading -->
 
  <!-- End Page Loading -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 

  </div>
  <!-- END TOP -->

<!-- START SIDEBAR -->
   <?php 
  
session_start();


 
  // cek apakah yang mengakses halaman ini sudah login
  if($_SESSION['id_level']==""){
    header("location:../../index.php?pesan=gagal");
  }
 
  ?>
  

  <?php include "page/header.php"; ?>
      <!-- START SIDEBAR -->
  <?php include "page/sidebar.php"; ?>


<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Data Inventaris</h1>
      <ol class="breadcrumb">
        <li><a href="index.html">Dashboard</a></li>
        <li><a href="#">Data inventaris</a></li>
        
      </ol>


  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">

    <!-- Start Panel -->
    <div class="col-md-12">
      <div class="panel panel-default">
      <?php
      include "koneksi.php";

$id_inventaris=$_GET['id_inventaris'];
$select=mysqli_query($conn,"select * from inventaris where id_inventaris='$id_inventaris'");
$tampil=mysqli_fetch_array($select);
?>  

<div class="row">
   <div class="col-md-15">
      <div class="panel panel-widget">
            <form method="post" action="update_inventaris.php?id_inventaris=<?php echo $id_inventaris;?>">
  <div class="form-group">
    <label >Nama</label>
    <input type="text" name="nama" value="<?php echo $tampil['nama']; ?>" class="form-control" >
  </div>
  <div class="form-group">
    <label >Kode</label>
    <input type="text" name="kode_inventaris" value="<?php echo $tampil['kode_inventaris']; ?>" class="form-control"  >
  </div> 

  <?php
        include "koneksi.php";
        $result = mysqli_query($conn,"select * from jenis order by id_jenis asc ");
        $jsArray = "var id_jenis = new Array();\n";
        ?>  
        <label>Jenis</label>
        <select class="form-control" name="id_jenis" onchange="changeValue(this.value)">
        <option selected="selected"><?php echo $tampil['id_jenis']; ?>
        <?php 
        while($row = mysqli_fetch_array($result)){
                    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
          $jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
        }
                  ?>
        </option>
        </select> 

  <?php
  include "koneksi.php";
  $result = mysqli_query($conn,"select * from ruang order by id_ruang asc ");
  $jsArray = "var id_ruang = new Array();\n";
  ?>  
  <label>Ruang</label>
  <select class="form-control" name="id_ruang" onchange="changeValue(this.value)">
  <option selected="selected"><?php echo $tampil['id_ruang']; ?>
   <?php 
   while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
        }
                  ?>
        </option>
        </select> 
         <?php
  include "koneksi.php";
  $result = mysqli_query($conn,"select * from petugas order by id_petugas asc ");
  $jsArray = "var id_petugas = new Array();\n";
  ?>  
  <label>Ruang</label>
  <select class="form-control" name="id_petugas" onchange="changeValue(this.value)">
  <option selected="selected"><?php echo $tampil['id_petugas']; ?>
   <?php 
   while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
    $jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
        }
                  ?>
        </option>
        </select> 
  <div class="form-group">
    <label >Jumlah</label>
    <input type="text" name="jumlah" value="<?php echo $tampil['jumlah']; ?>" class="form-control" >
  </div>
  <div class="form-group">
    <label >Tanggal</label>
    <input type="date" name="tanggal_register" value="<?php echo $tampil['tanggal_register']; ?>" class="form-control" >
  </div>
  <div class="form-group">
    <label>Kondisi</label>
    <input type="text" name="kondisi" value="<?php echo $tampil['kondisi']; ?>" class="form-control" >
  </div>
    <div class="form-group">
    <label>Sumber</label>
    <input type="text" name="sumber" value="<?php echo $tampil['sumber']; ?>" class="form-control">
  </div>
  <div class="form-group">
    <label>Keterangan</label>
    <input type="text" name="keterangan" value="<?php echo $tampil['keterangan']; ?>" class="form-control">
  </div>
  
    
  <button type="submit" class="btn btn-primary" name="simpan" value="simpan">Save changes</button>
</form>
                      </div>
                    </div>
</div>

      </div>
    </div>
    <!-- End Panel -->

  </div>
  <!-- End Row -->
</div>

  <?php include "page/footer.php"; ?>



</div>
<!-- End Content -->
 
<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>
<script type="text/javascript"> 
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
</script>


<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>


</body>

</html>