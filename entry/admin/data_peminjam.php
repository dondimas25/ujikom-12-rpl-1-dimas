<?php include "koneksi.php" ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Beranda || Sistem Untuk Keperluan Inventaris</title>

  <!-- ========== Css Files ========== -->
  <link href="css/root.css" rel="stylesheet">

  </head>
  <body>
  <!-- Start Page Loading -->
 
  <!-- End Page Loading -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
      <?php 
  
session_start();


 
  // cek apakah yang mengakses halaman ini sudah login
  if($_SESSION['id_level']==""){
    header("location:../../index.php?pesan=gagal");
  }
 
  ?>
  
  

<!-- START SIDEBAR -->


  <?php include "page/header.php"; ?>
      <!-- START SIDEBAR -->
  <?php include "page/sidebar.php"; ?>


<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Data Inventaris</h1>
      <ol class="breadcrumb">
        <li><a href="index.html">Dashboard</a></li>
        <li><a href="#">Data peminjam</a></li>
        
      </ol>
  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
   <div class="row">
   <div class="col-md-12 ">
      <div class="panel panel-widget">
        <div class="panel-title">
          Data Peminjam
          <ul class="panel-tools">
            <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
            <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
            <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
          </ul>
        </div>
        <div class="panel-body table-responsive">

            <table id="example0" class="table display">
                <thead>
                  <tr>
                  
                    <td align="center">No</td>
                    <td align="center">Id peminjam</td>
                    <td align="center">tanggal pinjam</td>
                    <td align="center">Tanggal Kembali</td>
                    <td align="center">Status Peminjaman</td>
                    <td align="center">Nama peminjam</td>
                    <td align="center">Aksi</td>
                  </tr>
                </thead>
        <tbody>
                                         <?php
                                    include "koneksi.php";
                                    $no=1;
                                   $select = mysqli_query($conn,"SELECT * FROM peminjaman INNER JOIN pegawai ON peminjaman.id_pegawai = pegawai.id_pegawai ORDER BY id_peminjaman desc ");
                                   $i = 1;
                                   while ($data = mysqli_fetch_array($select)){
                                    ?>
                                        <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['id_peminjaman']; ?></td>
                                        <td><?php echo $data['tanggal_pinjam']; ?></td>
                                        <td><?php echo $data['tanggal_kembali']; ?></td>
                                        <td><?php echo $data['status_peminjaman']; ?></td>
                                        <td><?php echo $data['nama_pegawai']; ?></td>
                                        <td align="center">
 <a href="data_peminjaman_detail.php?id_peminjaman=<?php echo $data['id_peminjaman'];?> class="btn btn-rounded btn-danger btn-icon"><i class="fa fa-eye"></i></a>
                      </td>
                                        </tr>
                                        <?php 
                                    }
                                    ?>
                                    </tbody>

            </table>


        
      
    </div>
    <!-- End Panel -->

  </div>

  <?php include "page/footer.php"; ?>



</div>
<!-- End Content -->
 
<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>



<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="10">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
} );
</script>

<script type="text/javascript"> 
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
</script>
</body>

</html>