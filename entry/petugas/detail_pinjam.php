
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Suki || Sistem Untuk Keperluan Inventaris</title>

  <!-- ========== Css Files ========== -->
  <link href="css/root.css" rel="stylesheet">
  <link href="css/sweetalert.css" rel="stylesheet">
  </head>
  <body>
  <!-- Start Page Loading -->
 
  <!-- START SIDEBAR -->
   <?php 
  
session_start();


 
  // cek apakah yang mengakses halaman ini sudah login
  if($_SESSION['id_level']==""){
    header("location:../../index.php?pesan=gagal");
  }
 
  ?>
  

  <?php include "page/header.php"; ?>
      <!-- START SIDEBAR -->
  <?php include "page/sidebar.php"; ?>


<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Tambah Inventaris</h1>
      <ol class="breadcrumb">
        <li><a href="home">Dashboard</a></li>
        <li><a href="#">Data inventaris</a></li>
        <li><a href="#">Tambah inventaris</a></li>
        
      </ol>


  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
      
<div class="row">
   <div class="col-md-6">
      <div class="panel panel-widget">
<form method="post" action="proses/proses_detail_pinjam.php">
 <?php
  include "koneksi.php";
  $result = mysqli_query($conn,"select * from inventaris order by id_inventaris asc ");
  $jsArray = "var id_inventaris = new Array();\n";
  ?>  
 <label>Nama inventaris</label>
  <select class="form-control" name="id_inventaris" onchange="changeValue(this.value)" required="">
  <option selected="selected">
   <?php 
   while($row = mysqli_fetch_array($result)){
    echo "<option value='$row[0].$row[1]'>$row[0]. $row[2]</option>";
    $jsArray .= "id_inventaris['". $row['id_inventaris']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
        }
                  ?>
        </option>
        </select>
 

   <div class="form-group">
    <label >Jumlah</label>
    <input type="number" name="jumlahp"   class="form-control" required="" >
     
  </div>
  <div class="form-group">
    <label >ID Peminjaman </label>
   <?php 
        $id_peminjaman = $_GET['id_peminjaman'];
        include"koneksi.php";
        $select=mysqli_query($conn, "SELECT * FROM peminjaman where id_peminjaman='$id_peminjaman'");
        while ($show=mysqli_fetch_array($select)) {
        
    ?>
    <input type="text" name="id_peminjaman" value="<?php echo $show['id_peminjaman']?>"" class="form-control" required="" readonly>
  </div><?php } ?>
 
    <br>
  <button type="submit" class="btn btn-primary"  value="simpan">Save changes</button>
</form>
                      </div>
                    </div>

 <div class="col-md-1 col-lg-6">
      <div class="panel panel-widget" style="height:450px;">
        <div class="panel-title">
          Data Barang  <span class="label label-danger"></span>
          <ul class="panel-tools">
            <li><a class="icon"><i class="fa fa-refresh"></i></a></li>
            <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
          </ul>
        </div>
        <div class="panel-body table-responsive">

          <table class="table table-dic table-hover ">
            <tbody>
                 <table id="example0" class="table display">
                <thead>
                  <tr>
                  
                    <td align="center">Kode Inventaris</td>
                    <td align="center">Nama Barang</td>
                    <td align="center">Jumlah</td>
                  
                  </tr>

<?php
              $no= 1;
              $ambil_data="SELECT * from inventaris order by id_inventaris desc";
              $sql = mysqli_query($conn, $ambil_data);
              while ($hasil = mysqli_fetch_array($sql)) 
              {
?>
             </thead>
                
                    <tr>
                      
                      <td align="center"><?php echo $hasil['kode_inventaris'] ?></td>
                      <td align="center"><?php echo $hasil['nama'] ?></td>
                      <td align="center"><?php echo $hasil['jumlah'] ?></td>
                    
                      
                    </tr>
 
                 <?php
  }
  ?>

            </table>
           
            </tbody>
          </table>          

        </div>
      </div>
    </div>

</div>

      
    
   
  <!-- End Row -->
</div>

  <?php include "page/footer.php"; ?>



</div>
<!-- End Content -->
 
 <script type="text/javascript" src="../js/sweetalert.js"></script>
<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>
<script type="text/javascript" src="js/sweetalert.js"></script>


<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>
<script type="text/javascript"> 
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
</script>


<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>


</body>

</html>