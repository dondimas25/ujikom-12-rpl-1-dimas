<?php include "koneksi.php" ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Beranda || Sistem Untuk Keperluan Inventaris</title>

  <!-- ========== Css Files ========== -->
  <link href="css/root.css" rel="stylesheet">

  </head>
  <body>
  <!-- Start Page Loading -->
  <div class="loading"><img src="img/load.gif" alt="loading-img"></div>
  <!-- End Page Loading -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 
      <?php 
  
session_start();


 
  // cek apakah yang mengakses halaman ini sudah login
  if($_SESSION['id_level']==""){
    header("location:../../index.php?pesan=gagal");
  }
 
  ?>
  
  

<!-- START SIDEBAR -->


  <?php include "page/header.php"; ?>
      <!-- START SIDEBAR -->
  <?php include "page/sidebar.php"; ?>


<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Data Inventaris</h1>
      <ol class="breadcrumb">
        <li><a href="index.html">Dashboard</a></li>
        <li><a href="#">Data inventaris</a></li>
        
      </ol>
  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
   <div class="row">
   <div class="col-md-12 ">
      <div class="panel panel-widget">
        <div class="panel-title">
          Data Inventaris
          <ul class="panel-tools">
            <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
            <li><a class="icon expand-tool"><i class="fa fa-expand"></i></a></li>
            <li><a class="icon closed-tool"><i class="fa fa-times"></i></a></li>
          </ul>
        </div>
        <div class="panel-body table-responsive">

          <a href="tambah_inventaris.php" class="btn btn-danger"> Tambah</a>
          <a href="export_excel_inven.php" target="_blank">
                <button class="btn btn-default"><i class="fa fa-print">Export Excel</i></button>
          </a>

          <br><br>
            <table id="example" class="table display">
                <thead>
                  <tr>
                  
                    <td align="center">Kode Inventaris</td>
                    <td align="center">Nama Barang</td>
                    <td align="center">Jenis</td>
                    <td align="center">Spesifikasi</td>
                    <td align="center">Kondisi</td>
                    <td align="center">Ruang</td>
                    <td align="center">Tanggal Register</td>
                    <td align="center">Petugas</td>
                    <td align="center">Sumber</td>
                     <td align="center">Keterangan</td>
                    <td align="center">Opsi</td>
                  </tr>

<?php
              $no= 1;
              $ambil_data="SELECT * from inventaris   ORDER BY id_inventaris desc";
              $sql = mysqli_query($conn, $ambil_data);
              while ($hasil = mysqli_fetch_array($sql)) 
              {
?>
             </thead>
                
                    <tr>
                      
                      <td align="center"><?php echo $hasil['kode_inventaris'] ?></td>
                      <td align="center"><?php echo $hasil['nama'] ?></td>
                      <td align="center"><?php echo $hasil['id_jenis'] ?></td>
                      <td align="center"><?php echo $hasil['spesifikasi'] ?></td>
                      <td align="center"><?php echo $hasil['kondisi'] ?></td>
                      <td align="center"><?php echo $hasil['id_ruang'] ?></td>
                       <td align="center"><?php echo $hasil['tanggal_register'] ?></td>
                      <td align="center"><?php echo $hasil['id_petugas'] ?></td>
                       <td align="center"><?php echo $hasil['sumber'] ?></td>
                       <td align="center"><?php echo $hasil['keterangan'] ?></td>
                      <td align="center">
 <a href="edit_inventaris?id_inventaris=<?php echo $hasil['id_inventaris']; ?>" class="btn btn-rounded btn-danger btn-icon"><i class="fa fa-edit"></i></a>
                      </td>
                    </tr>
 
                 <?php
  }
  ?>

            </table>


        </div>
      </div>
    </div>
    <!-- End Panel -->

  </div>

  <?php include "page/footer.php"; ?>



</div>
<!-- End Content -->
 
<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>



<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="10">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
} );
</script>

<script type="text/javascript"> 
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
</script>
</body>

</html>