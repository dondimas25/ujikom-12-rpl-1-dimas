
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Beranda || Sistem Untuk Keperluan Inventaris</title>

  <!-- ========== Css Files ========== -->
  <link href="../css/root.css" rel="stylesheet">

  </head>
  <body>
  <!-- Start Page Loading -->
 <div class="loading"><img src="../../../img/loading.gif" alt="loading-img"></div>
 
  <!-- End Page Loading -->
 <!-- //////////////////////////////////////////////////////////////////////////// --> 

  </div>
  <!-- END TOP -->

<!-- START SIDEBAR -->


  <?php include "../page/header.php"; ?>
      <!-- START SIDEBAR -->
  <?php include "../page/sidebar.php"; ?>


<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Tambah ruang</h1>
      <ol class="breadcrumb">
        <li><a href="index.html">Dashboard</a></li>
        <li><a href="#">Data Ruang</a></li>
        <li><a href="#">Tambah Ruang</a></li>
        
      </ol>


  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">

    <!-- Start Panel -->
    <div class="col-md-12">
      <div class="panel panel-default"> 
<div class="row">
   <div class="col-md-15">
      <div class="panel panel-widget">
<form method="post" action="proses_tambah_ruang.php">
  <div class="form-group">
    <label >Nama Ruang</label>
    <input type="text" name="nama_ruang" class="form-control" required="" >
  </div>
  <?php 
      $koneksi = mysqli_connect('localhost','root','','inventaris_db');

      $cari_kd=mysqli_query($koneksi,"select max(kode_ruang) as kode from ruang");

      $tm_cari=mysqli_fetch_array($cari_kd);
      $kode=substr($tm_cari['kode'], 1,4);

      $tambah=$kode+1;

      if ($tambah<10) {
        $kode_ruang="R000".$tambah;
      }else{
        $kode_ruang="R00".$tambah;
      }

   ?>
  <div class="form-group">
    <label >Kode</label>
    <input type="text" name="kode_ruang" value="<?php echo $kode_ruang ?>" class="form-control"  readonly="">
  </div> 
  <div class="form-group">
    <label>Keterangan</label>
    <input type="text" name="keterangan" class="form-control" required="">
  </div>
  
    
  <button type="submit" class="btn btn-primary" value="simpan">Save changes</button>
</form>
                      </div>
                    </div>
</div>

      </div>
    </div>
    <!-- End Panel -->

  </div>
  <!-- End Row -->
</div>

  <?php include "../page/footer.php"; ?>



</div>
<!-- End Content -->
 
<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>

<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>
<script type="text/javascript"> 
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
</script>


<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>


</body>

</html>