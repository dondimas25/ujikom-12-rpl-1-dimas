<div class="col-md-12">
      <div>
              <div class="panel panel-default">
        <div class="panel-body table-responsive">
        	<a href="tambah_inventaris.php" class="btn btn-danger"> Tambah</a><br><br>
            <table id="example0" class="table display">
                <thead>
                  <tr>
                    <td align="center">No</td>
                    <td align="center">Kode Inventaris</td>
                    <td align="center">Nama Barang</td>
                    <td align="center">Jenis</td>
                    <td align="center">Kondisi</td>
                    <td align="center">Ruang</td>
                    <td align="center">Tanggal Register</td>
                    <td align="center">Petugas</td>
                    <td align="center">Sumber</td>
                     <td align="center">Keterangan</td>
                    <td align="center">Opsi</td>
                  </tr>

<?php
              $no= 1;
              $ambil_data="SELECT * from inventaris  order by id_inventaris desc";
              $sql = mysqli_query($conn, $ambil_data);
              while ($hasil = mysqli_fetch_array($sql)) 
              {
?>
             </thead>
                
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td align="center"><?php echo $hasil['kode_inventaris'] ?></td>
                      <td align="center"><?php echo $hasil['nama'] ?></td>
                      <td align="center"><?php echo $hasil['id_jenis'] ?></td>
                      <td align="center"><?php echo $hasil['kondisi'] ?></td>
                      <td align="center"><?php echo $hasil['id_ruang'] ?></td>
                       <td align="center"><?php echo $hasil['tanggal_register'] ?></td>
                      <td align="center"><?php echo $hasil['id_petugas'] ?></td>
                       <td align="center"><?php echo $hasil['sumber'] ?></td>
                       <td align="center"><?php echo $hasil['keterangan'] ?></td>
                      <td align="center">
 <a href="edit_inventaris?id_inventaris=<?php echo $hasil['id_inventaris']; ?>" class="btn btn-rounded btn-danger btn-icon"><i class="fa fa-edit"></i></a>
                      </td>
                    </tr>
 
                 <?php
  }
  ?>

            </table>


        </div>

      </div>
    </div>
    <!-- End Panel -->

  </div>