
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Suki || Sistem Untuk Keperluan Inventaris</title>

  <!-- ========== Css Files ========== -->
  <link href="css/root.css" rel="stylesheet">
 
    <link rel="stylesheet" href="./css/bootstrap-material-datetimepicker.css" />
   
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/ripples.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/material.min.js"></script>
    <script type="text/javascript" src="https://rawgit.com/FezVrasta/bootstrap-material-design/master/dist/js/material.min.js"></script>
    <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap-material-datetimepicker.js"></script>

  </head>
  <body>
  <!-- Start Page Loading -->
  
  <!-- START SIDEBAR -->


  <?php include "page/header.php"; ?>
      <!-- START SIDEBAR -->
  <?php include "page/sidebar.php"; ?>


<!-- START CONTENT -->
<div class="content">

  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">Tambah Inventaris</h1>
      <ol class="breadcrumb">
        <li><a href="home">Dashboard</a></li>
        <li><a href="#">Data inventaris</a></li>
        <li><a href="#">Tambah inventaris</a></li>
        
      </ol>


  </div>
  <!-- End Page Header -->

 <!-- //////////////////////////////////////////////////////////////////////////// --> 
<!-- START CONTAINER -->
<div class="container-padding">


  <!-- Start Row -->
  <div class="row">

    <!-- Start Panel -->
    <div class="col-md-12">
      <div class="panel panel-default"> 
<div class="row">
   <div class="col-md-15">
      <div class="panel panel-widget">
<form method="post" action="proses/proses_pinjam.php">
  <?php
        include "koneksi.php";
        $result = mysqli_query($conn,"select * from pegawai order by id_pegawai asc ");
        $jsArray = "var id_pegawai = new Array();\n";
        ?>  
        <label>Id pegawai</label>
        <select class="form-control" name="id_pegawai" onchange="changeValue(this.value)" required="">
        <option selected="selected">
        <?php 
        while($row = mysqli_fetch_array($result)){
                    echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
          $jsArray .= "id_pegawai['". $row['id_pegawai']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
        }
                  ?>
        </option>
        </select><br>
  <div class="form-group">
    <label >Tanggal</label>
    <input type="text" name="tanggal_pinjam" value="<?php echo date('y-m-d ')?>" class="form-control" required="" readonly="">
     <small id="tglkod" class="text-muted">
      Format (tahun - bulan - hari)
    </small>
  </div>
    <div class="form-group">
    <label >Tanggal Kembali</label>
    <input type="text" name="tanggal_kembali"  id="min-date" class="form-control" required="" >
     <small id="tglkod" class="text-muted">
      Format (hari - bulan - tahun)
    </small>
  </div>
 
    <br>
  <button type="submit" class="btn btn-primary"  value="simpan">Simpan</button>
</form>
                      </div>
                    </div>
</div>

      </div>
    </div>
    <!-- End Panel -->

  </div>
  <!-- End Row -->
</div>

  <?php include "page/footer.php"; ?>



</div>
<!-- End Content -->
 <script type="text/javascript">
    $(document).ready(function()
    {
      $('#date').bootstrapMaterialDatePicker
      ({
        time: false,
        clearButton: true
      });

      $('#time').bootstrapMaterialDatePicker
      ({
        date: false,
        shortTime: false,
        format: 'HH:mm'
      });

      $('#date-format').bootstrapMaterialDatePicker
      ({
        format: 'dddd DD MMMM YYYY - HH:mm'
      });
      $('#date-fr').bootstrapMaterialDatePicker
      ({
        format: 'DD/MM/YYYY HH:mm',
        lang: 'fr',
        weekStart: 1, 
        cancelText : 'ANNULER',
        nowButton : true,
        switchOnClick : true
      });

      $('#date-end').bootstrapMaterialDatePicker
      ({
        weekStart: 0, format: 'DD/MM/YYYY HH:mm'
      });
      $('#date-start').bootstrapMaterialDatePicker
      ({
        weekStart: 0, format: 'DD/MM/YYYY HH:mm', shortTime : true
      }).on('change', function(e, date)
      {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
      });

      $('#min-date').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY ',time: false, minDate : new Date() });

      $.material.init()
    });
    </script>
 
<!-- ================================================
jQuery Library
================================================ -->

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="js/bootstrap/bootstrap.min.js"></script>


<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="js/plugins.js"></script>


<!-- ================================================
Data Tables
================================================ -->
<script src="js/datatables/datatables.min.js"></script>

<script type="text/javascript"> 
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
</script>


<script>
$(document).ready(function() {
    $('#example0').DataTable();
} );
</script>





</body>

</html>